FROM openjdk:11-jre-slim

COPY ./build/libs/shoppingcart-service-*.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]