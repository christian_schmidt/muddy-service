package component

import de.tarent.ciwanzik.Application
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [Application])
class AddProductCartTest extends Specification {

    static final JsonSlurper json = new JsonSlurper()

    @Autowired
    TestRestTemplate restTemplate

    def "a product can be added to an existing shopping cart"() {
        given: "An existing shopping cart"
        def String location = getAddProductLink(createAndGetShoppingCart())

        when: "Adding a product"
        def request = """ { "sku": "2035", "quantity": 2 } """
        def response = addProductToShoppingCart(location, request)

        then: "The response is http 200"
        response.statusCodeValue == 200

        and: "The response has the current shopping cart"
        def shoppingCart = json.parseText(response.body)

        shoppingCart.amount == 2 * 499
        shoppingCart.items.size == 1
        shoppingCart.items[0].quantity == 2
        shoppingCart.items[0].product.sku == "2035"
        shoppingCart.items[0].product.name == "Brot"
        shoppingCart.items[0].product.price == 499
    }

    def "two products can be added to an existing shopping cart"() {
        given: "An existing shopping cart"
        def String shoppingCartLocation = createShoppingCart()
        def String addProductLocation = getAddProductLink(getShoppingCart(shoppingCartLocation))

        when: "Adding a product"
        def request = """ { "sku": "2035", "quantity": 2 } """
        addProductToShoppingCart(addProductLocation, request)

        and: "Adding a second product"
        request = """ { "sku": "102", "quantity": 3 } """
        addProductToShoppingCart(addProductLocation, request)

        then: "The current shopping cart has both products"
        def shoppingCart = getShoppingCart(shoppingCartLocation)

        shoppingCart.amount == 2 * 499 + 3 * 1299
        shoppingCart.items.size == 2

        and: "there is 'Brot"
        shoppingCart.items[0].quantity == 2
        shoppingCart.items[0].product.sku == "2035"
        shoppingCart.items[0].product.name == "Brot"
        shoppingCart.items[0].product.price == 499

        and: "there is 'Milch"
        shoppingCart.items[1].quantity == 3
        shoppingCart.items[1].product.sku == "102"
        shoppingCart.items[1].product.name == "Milch"
        shoppingCart.items[1].product.price == 1299
    }

    def "a quantity of 11 is a bad request"() {
        given: "An existing shopping cart"
        def String shoppingCartLocation = createShoppingCart()
        def String addProductLocation = getAddProductLink(getShoppingCart(shoppingCartLocation))

        when: "Adding a product 10 times"
        def request = """ { "sku": "2035", "quantity": 11 } """
        def response = addProductToShoppingCart(addProductLocation, request)

        then: "We got an http 400"
        response.statusCodeValue == 400

        and: "The shopping cart has still an amount of 0"
        def shoppingCart = getShoppingCart(shoppingCartLocation)
        shoppingCart.amount == 0
        shoppingCart.items.size == 0
    }

    def "a quantity of 11 of two requests is a bad request"() {
        given: "An existing shopping cart"
        def String shoppingCartLocation = createShoppingCart()
        def String addProductLocation = getAddProductLink(getShoppingCart(shoppingCartLocation))

        when: "Adding a product"
        def request = """ { "sku": "2035", "quantity": 10 } """
        def response = addProductToShoppingCart(addProductLocation, request)

        and: "Adding the same product again"
        request = """ { "sku": "2035", "quantity": 3 } """
        addProductToShoppingCart(addProductLocation, request)

        then: "We got an http 400"
        response.statusCodeValue == 200

        and: "The shopping cart has still an amount of 0"
        def shoppingCart = getShoppingCart(shoppingCartLocation)
        shoppingCart.amount == 4990
        shoppingCart.items.size == 1
    }

    def "max amount is 200,00 €"() {
        given: "An existing shopping cart"
        def String shoppingCartLocation = createShoppingCart()
        def String addProductLocation = getAddProductLink(getShoppingCart(shoppingCartLocation))

        when: "Adding a product -> amount 200€"
        def request = """ { "sku": "102", "quantity": 10 } """
        addProductToShoppingCart(addProductLocation, request)

        and: "a second product"
        request = """ { "sku": "S-155", "quantity": 10 } """
        def response = addProductToShoppingCart(addProductLocation, request)

        then: "We got an http 400"
        response.statusCodeValue == 400

        and: "The shopping cart has still an amount of 0"
        def shoppingCart = getShoppingCart(shoppingCartLocation)
        shoppingCart.amount == 12990
        shoppingCart.items.size == 1
    }

    def "a not existing product returns http 404"() {
        given: "An existing shopping cart"
        def String shoppingCartLocation = createShoppingCart()
        def String addProductLocation = getAddProductLink(getShoppingCart(shoppingCartLocation))

        when: "Adding a product"
        def request = """ { "sku": "unknwon", "quantity": 11 } """
        def response = addProductToShoppingCart(addProductLocation, request)

        then: "We got an http 400"
        response.statusCodeValue == 404
    }

    def "a not existing shopping cart returns http 404"() {
        when: "Adding a product to an unknwon shopping cart"
        def request = """ { "sku": "unknwon", "quantity": 11 } """
        def response = addProductToShoppingCart("/shoppingcart/UNKNOWN/items", request)

        then: "We got an http 400"
        response.statusCodeValue == 404
    }

    private def createShoppingCart() {
        def response = restTemplate.postForEntity("/shoppingcart", "", String.class)
        return response.headers.LOCATION[0];
    }

    private def createAndGetShoppingCart() {
        def location = createShoppingCart()
        return getShoppingCart(location)
    }

    private def getShoppingCart(location) {
        return json.parseText(restTemplate.getForObject(location, String.class))
    }

    private def getAddProductLink(shoppingCart) {
        return shoppingCart.links.addProduct.href
    }

    private def addProductToShoppingCart(String shoppingCartLocation, String productJson) {
        def header = new HttpHeaders()
        header.setContentType(MediaType.APPLICATION_JSON_UTF8)
        def entity = new HttpEntity(productJson, header)

        return restTemplate.exchange(shoppingCartLocation, HttpMethod.PUT, entity, String.class)
    }
}
