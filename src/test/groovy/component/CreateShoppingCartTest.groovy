package component

import de.tarent.ciwanzik.Application
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [Application])
class CreateShoppingCartTest extends Specification {

    @Autowired
    TestRestTemplate restTemplate

    def "a POST on /shoppingcart returns HTTP 201"() {
        when: "Making a POST"
        def response = restTemplate.postForEntity("/shoppingcart", "", String.class)

        then: "The HTTP response is 201"
        response.statusCodeValue == 201
    }

    def "a POST on /shoppingcart returns a Location URI"() {
        when: "Making a POST"
        def response = restTemplate.postForEntity("/shoppingcart", "", String.class)

        then: "There is a location header with the uuid"
        response.headers.LOCATION =~ /shoppingcart\/\w+/
    }

    def "a created shoppingcart can be received by a GET"() {
        given: "An existing shopping cart"
        def location = createShoppingCart()

        when: "Making a GET"
        def response = restTemplate.getForEntity(location, String.class)

        then: "The reponse is http 200"
        response.statusCodeValue == 200
    }

    def "a created shoppingcart has a self link"() {
        given: "An existing shopping cart"
        def location = createShoppingCart()

        when: "Making a GET"
        def shoppingCart = new JsonSlurper().parseText(restTemplate.getForObject(location, String.class))

        then: "There is a location header with the uuid"
        shoppingCart.links.self.name == "self"
        shoppingCart.links.self.href == location
        shoppingCart.links.self.method == "GET"
    }

    def "a created shoppingcart has a add product link"() {
        given: "An existing shopping cart"
        def location = createShoppingCart()

        when: "Making a GET"
        def shoppingCart = new JsonSlurper().parseText(restTemplate.getForObject(location, String.class))

        then: "There is a location header with the uuid"
        shoppingCart.links.addProduct.name == "addProduct"
        shoppingCart.links.addProduct.href == location + "/items"
        shoppingCart.links.addProduct.method == "PUT"
    }

    def "receiving a not existing shopping cart returns 404"() {
        given: "A not existing shopping cart location"
        def location = "/shoppingcart/123e4567-e89b-12d3-a456-426655440000"

        when: "Making a GET"
        def response = restTemplate.getForEntity(location, String.class)

        then: "http 404"
        response.statusCodeValue == 404
    }

    def "malformed uuid return 400"() {
        given: "A not existing shopping cart location"
        def location = "/shoppingcart/not_a_uuid"

        when: "Making a GET"
        def response = restTemplate.getForEntity(location, String.class)

        then: "http 400"
        response.statusCodeValue == 400
        response.body == null
    }

    private def createShoppingCart() {
        def response = restTemplate.postForEntity("/shoppingcart", "", String.class)
        return response.headers.LOCATION[0];
    }

}
