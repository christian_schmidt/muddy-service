package de.tarent.ciwanzik.peristence

import de.tarent.ciwanzik.model.ShoppingCart
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ShoppingCartRepo: JpaRepository<ShoppingCart, String>