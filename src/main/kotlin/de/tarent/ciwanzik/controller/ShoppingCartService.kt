package de.tarent.ciwanzik.controller

import de.tarent.ciwanzik.client.MockedProductServiceClient
import de.tarent.ciwanzik.client.ProductServiceClient
import de.tarent.ciwanzik.model.ShoppingCart
import de.tarent.ciwanzik.model.ShoppingCartItem
import de.tarent.ciwanzik.peristence.ShoppingCartRepo
import org.springframework.stereotype.Service
import java.util.*

@Service
open class ShoppingCartService(
    val shoppingCartRepo: ShoppingCartRepo, val productServiceClient: MockedProductServiceClient
) {

    fun createShoppingCart(): ShoppingCart {
        val uuid = UUID.randomUUID()
        val shoppingCart = ShoppingCart(uuid.toString(), 0, arrayListOf())

        shoppingCartRepo.save(shoppingCart)
        return shoppingCart
    }

    fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepo.findAll()
    }

    fun getShoppingCart(uuid: String): Optional<ShoppingCart> {
        return shoppingCartRepo.findById(uuid)
    }

    open fun addProduct(uuid: String, sku: String, quantity: Int): ShoppingCart? {
        val optionalCart = shoppingCartRepo.findById(uuid)

        return if(optionalCart.isPresent) {
            val cart = optionalCart.get()
            val product = productServiceClient.getProduct(sku)

            return if(product != null) {
                cart.items?.add(ShoppingCartItem(0, product.sku, product, quantity))
                cart.amount = calculateAmount(cart.items ?: listOf())
                shoppingCartRepo.save(cart)
                shoppingCartRepo.flush()
                cart
            } else {
                null
            }
        } else {
            null
        }
    }

    private fun calculateAmount(items: List<ShoppingCartItem>): Int {
        var amount: Int = 0

        for(item in items) {
            val price = item.product?.price ?: 0
            val quantity = item.quantity ?: 0
            amount += price * quantity
        }

        return amount
    }
}