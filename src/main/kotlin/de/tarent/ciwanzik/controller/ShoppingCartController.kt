package de.tarent.ciwanzik.controller

import de.tarent.ciwanzik.model.ShoppingCart
import de.tarent.ciwanzik.model.ShoppingCartItem
import org.slf4j.LoggerFactory
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.transaction.TransactionSystemException
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import java.net.URI
import java.util.*
import javax.validation.ConstraintViolationException

@Controller
class ShoppingCartController(val service: ShoppingCartService) {

    val logger = LoggerFactory.getLogger(ShoppingCartController::class.java)

    @RequestMapping(path = ["/shoppingcart"], method = [RequestMethod.POST])
    fun postShoppingCart(): ResponseEntity<ShoppingCart> {
        val shoppingCart = service.createShoppingCart()
        return ResponseEntity.created(getShoppingCartLink(shoppingCart)).build()
    }

    @RequestMapping(path = ["/shoppingcart"], method = [RequestMethod.GET])
    fun getShoppingCarts(): ResponseEntity<List<ShoppingCart>> {
        return ResponseEntity.ok(service.getShoppingCarts())
    }

    @RequestMapping(path = ["/shoppingcart/{stringUuid}"], method = [RequestMethod.GET])
    fun getShoppingCart(@PathVariable stringUuid: String): ResponseEntity<ShoppingCart> {
        return try {
            val uuid = UUID.fromString(stringUuid)
            val optional = service.getShoppingCart(uuid.toString())

            if (optional.isPresent) {
                val cart = optional.get()
                cart.addLink("self", "GET", getShoppingCartLink(cart))
                cart.addLink("addProduct", "PUT", URI("/shoppingcart/${cart.uuid}/items"))
                ResponseEntity.ok(cart)
            } else {
                ResponseEntity.notFound().build()
            }
        } catch (e: Exception) {
            ResponseEntity.badRequest().build()
        }
    }

    @RequestMapping(path = ["/shoppingcart/{uuid}/items"], method = [RequestMethod.PUT])
    fun addProduct(@PathVariable uuid: String, @RequestBody product: ShoppingCartItem): ResponseEntity<*> {
        return try {
            val skuRegex = "[\\w\\d]{1,20}".toRegex()
            val sku = product.sku ?: ""
            val quantiy = product.quantity ?: 0

            return if (sku != null && skuRegex.matches(sku)) {
                logger.info("Adding product $sku to shopping cart $uuid")
                val cart = service.addProduct(uuid, sku, quantiy)

                if (cart != null) {
                    cart.addLink("self", "GET", getShoppingCartLink(cart))
                    cart.addLink("addProduct", "PUT", URI("/shoppingcart/${cart.uuid}/items"))
                    ResponseEntity.ok(cart)
                } else {
                    ResponseEntity.notFound().build()
                }
            } else {
                ResponseEntity.badRequest().body("sku is not valid")
            }
        } catch (e: Exception) {
            when(e) {
                is TransactionSystemException,
                is DataIntegrityViolationException -> ResponseEntity.badRequest().body("Constraint validated")
                else -> throw e
            }

        }
    }

    private fun getShoppingCartLink(shoppingCart: ShoppingCart): URI {
        return URI("/shoppingcart/${shoppingCart.uuid}")
    }
}