package de.tarent.ciwanzik.model

import javax.persistence.CascadeType.ALL
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.validation.constraints.Max

@Entity
data class ShoppingCart (
    @Id
    var uuid: String?,

    @get:Max(value = 200_00)
    var amount: Int?,

    @OneToMany(cascade = [ALL])
    var items: MutableList<ShoppingCartItem>?
): RestLinked<ShoppingCart>()