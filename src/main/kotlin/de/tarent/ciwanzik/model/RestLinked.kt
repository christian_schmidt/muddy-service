package de.tarent.ciwanzik.model

import java.net.URI
import javax.persistence.Transient

abstract class RestLinked<out T : RestLinked<T>> {
    @Transient
    val links: MutableMap<String, Link> = mutableMapOf()

    @Suppress("UNCHECKED_CAST")
    fun addLink(name: String, method: String, href: URI): T {
        links[name] = Link(name, method, href.toString())
        return this as T
    }
}

data class Link(val name: String, val method: String, val href: String)