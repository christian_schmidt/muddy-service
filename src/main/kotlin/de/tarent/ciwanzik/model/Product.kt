package de.tarent.ciwanzik.model

import javax.annotation.RegEx
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

@Entity
data class Product(
    @Id
    var sku: String?,

    var name: String?,

    var price: Int?,

    @ElementCollection
    @CollectionTable(
        name="ean"
    )
    var eans: List<String>?
)