package de.tarent.ciwanzik.model

import javax.persistence.CascadeType.ALL
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class ShoppingCartItem(
    @Id
    @GeneratedValue
    var id: Int?,

    var sku: String?,

    @OneToOne(cascade = [ALL])
    var product: Product?,

    var quantity: Int?
)