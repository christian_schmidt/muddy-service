package de.tarent.ciwanzik.client

import de.tarent.ciwanzik.model.Product
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class ProductServiceClient(val client: RestTemplate, val properties: ProductServiceProperties) {

    val logger = LoggerFactory.getLogger(ProductServiceClient::class.java)

    fun getProduct(sku: String): Product? {
        val url = "${properties.host}:${properties.port}/products/$sku"
        logger.info("Calling produt service on URL $url")
        val response: ResponseEntity<Product> = client.getForEntity(url, Product::class.java)

        return if(response.statusCode == HttpStatus.OK) {
            response.body
        } else {
            null
        }
    }
}