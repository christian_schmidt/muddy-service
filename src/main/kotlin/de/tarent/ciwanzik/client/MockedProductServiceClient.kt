package de.tarent.ciwanzik.client

import de.tarent.ciwanzik.model.Product
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.util.*

@Service
class MockedProductServiceClient(val client: RestTemplate, val properties: ProductServiceClient) {

    fun getProduct(sku: String): Product? {
        return when(sku) {
            "102" -> Product("102", "Milch", 1299, listOf("12345678", "77777777", "23498128"))
            "2035" -> Product("2035", "Brot", 499, listOf("34558821", "12323410"))
            "S-155" -> Product("S-155", "Käse", 799, listOf("34598146", "43565922", "23454045"))
            else -> null
        }
    }
}