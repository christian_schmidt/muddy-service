package de.tarent.ciwanzik.client

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "service.product")
open class ProductServiceProperties {
    lateinit var host: String
    lateinit var port: String
}